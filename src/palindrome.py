class Solution:
    def solution(self, word: str) -> bool:
        word = word.lower()
        word = word.replace(" ", "")

        if not word or word[0] != word[-1]:
            return False

        original = word
        # word = word[len(word)::-1]
        word = ""
        i = len(original)
        while i > 0:
            word += original[i - 1]
            i -= 1

        return word == original
